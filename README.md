Repository destined to the generation of the model of the building "Instituto Tecnológico de Sistemas de la Información" of Ciudad Real according to the IndoorGML OGC standard.

----------
# Notes
## Tools
Tools used for viewing and editing the model.
### Visualizer
* **Name:** JInedit
* **Repo:** https://github.com/STEMLab/IndoorGML-Viewer


### Editor
* **Name:** IndorGML-Viewer
* **Repo:** https://github.com/STEMLab/JInedit

#### Java verion

```
ruben@skynet:~$ java -version
java version "1.6.0_45"
Java(TM) SE Runtime Environment (build 1.6.0_45-b06)
Java HotSpot(TM) 64-Bit Server VM (build 20.45-b01, mixed mode)
```

```
ruben@skynet:~/repo/JInedit$ export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/

```

## How to work with more than two layers with the JInedit editor
1. Create an additional *layer*.
2. Create a new element *floor* so that the rest of layers/floors do not hinder the display *(Note: remember that the layer elements is something internal to the editor, since the model resulting -file .gml- will not appear.)*.
3. Create the new *cell*, *state* and *transition* elements in the *layer* element you just created.

In the structure of the .gml file, all *cell* type elements will appear together without any separation (it only refers to its *state* corresponding element through duality -the latter if they are already associated to *layer*- elements). On the other hand, the *state* and *transition* elements will be grouped by *layer*.

## Corrector ##
As the .gml file exported by the JInedit editor introduces redundant *cellSpaceBoundaryMember* and *transitionMember* (and their associated references through duality) elements, a *script* has been implemented that eliminates this redundancy.

----------
# TODO
- [x] Model topology cells.
- [x] Model topology states.
- [x] Model doors.
- [x] Model topology transitions.
- [x] Name cells.
- [x] Name states.
- [ ] Name transitions.
- [ ] Name doors.
- [x] Redundancy corrector script.
- [x] Images of both floors with names.
- [ ] Naming (in gml and in photos) concierge and the room next door.
- [x] Add by hand the connection of the two stairs and the elevator (between floors).


----------
# TOFIX
- [x] Correct corrector script (when there are several spaceLayers, just take the first one from the tree).

----------
