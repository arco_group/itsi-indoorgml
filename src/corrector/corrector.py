import sys
import xml.etree.ElementTree as ET

ET.register_namespace('', "http://www.opengis.net/indoorgml/1.0/core")
ET.register_namespace('gml', "http://www.opengis.net/gml/3.2")
ET.register_namespace('xlink', "http://www.w3.org/1999/xlink")
ET.register_namespace('xsi', "http://www.w3.org/2001/XMLSchema-instance")
ET.register_namespace('xsi:schemaLocation',
                      "http://www.opengis.net/indoorgml/1.0/core http://schemas.opengis.net/indoorgml/1.0/indoorgmlcore.xsd")
ET.register_namespace('gml:id', "IFs")

ns = {'indoorgml': 'http://www.opengis.net/indoorgml/1.0/core',
      'gml': 'http://www.opengis.net/gml/3.2'}

eq = {}
parents = {}
transitions = []
transitionMembers = []
stateMembers = []

file = sys.argv[1]
tree = ET.parse(file)
root = tree.getroot()
PrimalSpaceFeatures = root.find('indoorgml:primalSpaceFeatures', ns).find(
    'indoorgml:PrimalSpaceFeatures', ns)
cellSpaceBoundaryMembers = PrimalSpaceFeatures.findall(
    'indoorgml:cellSpaceBoundaryMember', ns)
spaceLayerMembers = root.find('indoorgml:MultiLayeredGraph', ns).find(
    'indoorgml:spaceLayers', ns).findall('indoorgml:spaceLayerMember', ns)


for cellSpaceBoundaryMember in PrimalSpaceFeatures.findall('indoorgml:cellSpaceBoundaryMember', ns):
    geometry3D = cellSpaceBoundaryMember.find(
        'indoorgml:CellSpaceBoundary', ns).find('indoorgml:geometry3D', ns)

    if(geometry3D.find('gml:OrientableSurface', ns)):
        ref = geometry3D.find('gml:OrientableSurface', ns).find(
            'gml:baseSurface', ns).get('{http://www.w3.org/1999/xlink}href')[1:]

        for csbm in PrimalSpaceFeatures.findall('indoorgml:cellSpaceBoundaryMember', ns):
            name = csbm.find('indoorgml:CellSpaceBoundary', ns).find(
                'indoorgml:geometry3D', ns).find('gml:Polygon', ns).find('gml:name', ns).text

            if(ref == name):
                eq[cellSpaceBoundaryMember.find('indoorgml:CellSpaceBoundary', ns).find(
                    'gml:name', ns).text] = csbm.find('indoorgml:CellSpaceBoundary', ns).find('gml:name', ns).text
                break

        duality = cellSpaceBoundaryMember.find(
            'indoorgml:CellSpaceBoundary', ns).find('indoorgml:duality', ns)
        if duality is not None:
            transitions.append(duality.get(
                '{http://www.w3.org/1999/xlink}href')[1:])

        PrimalSpaceFeatures.remove(cellSpaceBoundaryMember)

for cellSpaceMember in PrimalSpaceFeatures.findall('indoorgml:cellSpaceMember', ns):
    partialboundedByS = cellSpaceMember.find(
        'indoorgml:CellSpace', ns).findall('indoorgml:partialboundedBy', ns)

    for partialboundedBy in partialboundedByS:
        if partialboundedBy.get('{http://www.w3.org/1999/xlink}href')[1:] in eq:
            partialboundedBy.set('{http://www.w3.org/1999/xlink}href', '#' +
                                 eq[partialboundedBy.get('{http://www.w3.org/1999/xlink}href')[1:]])


for spaceLayerMember in spaceLayerMembers:
    edge = spaceLayerMember.find(
        'indoorgml:SpaceLayer', ns).find('indoorgml:edges', ns)
    transitionMembers.extend(edge.findall('indoorgml:transitionMember', ns))

    for transitionMember in edge.findall('indoorgml:transitionMember', ns):
        parents[transitionMember.find('indoorgml:Transition', ns).find(
            'gml:name', ns).text] = edge

    node = spaceLayerMember.find(
        'indoorgml:SpaceLayer', ns).find('indoorgml:nodes', ns)
    stateMembers.extend(node.findall('indoorgml:stateMember', ns))


for transition in transitions:
    for transitionMember in transitionMembers:
        transitionMemberName = transitionMember.find(
            'indoorgml:Transition', ns).find('gml:name', ns).text
        if transitionMemberName == transition:
            parents[transitionMemberName].remove(transitionMember)
            # edges.remove(transitionMember)

    for stateMember in stateMembers:
        connects = stateMember.find('indoorgml:State', ns).findall(
            'indoorgml:connects', ns)
        for connect in connects:
            if connect.get('{http://www.w3.org/1999/xlink}href')[1:] == transition:
                stateMember.find('indoorgml:State', ns).remove(connect)


print(eq)
print(transitions)
tree.write('./output.gml', encoding="UTF-8",  xml_declaration=True)
