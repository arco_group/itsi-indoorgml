<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<IndoorFeatures xmlns="http://www.opengis.net/indoorgml/1.0/core" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:ns4="http://www.opengis.net/indoorgml/1.0/navigation" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" gml:id="IFs" xsi:schemaLocation="http://www.opengis.net/indoorgml/1.0/core http://schemas.opengis.net/indoorgml/1.0/indoorgmlcore.xsd">
    <gml:name>IFs</gml:name>
    <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="EPSG::4326">
            <gml:lowerCorner>3.3974358974 7.1651090343 0.0</gml:lowerCorner>
            <gml:upperCorner>45.1282051282 43.8473520249 3.0</gml:upperCorner>
        </gml:Envelope>
    </gml:boundedBy>
    <primalSpaceFeatures>
        <PrimalSpaceFeatures gml:id="PS1">
            <gml:name>PS1</gml:name>
            <gml:boundedBy xsi:nil="true"/>
            <cellSpaceMember>
                <CellSpace gml:id="R1">
                    <gml:description>Usage=Room:Description=Cell R1</gml:description>
                    <gml:name>R1</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <Geometry3D>
                        <gml:Solid gml:id="SOLID1">
                            <gml:exterior>
                                <gml:Shell>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY3">
                                            <gml:name>POLY3</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>30.8974358974 43.8473520249 3.0</gml:pos>
                                                    <gml:pos>3.3974358974 43.8473520249 3.0</gml:pos>
                                                    <gml:pos>3.3974358974 17.6012461059 3.0</gml:pos>
                                                    <gml:pos>30.8974358974 17.6012461059 3.0</gml:pos>
                                                    <gml:pos>30.8974358974 43.8473520249 3.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY4">
                                            <gml:name>POLY4</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>30.8974358974 43.8473520249 0.0</gml:pos>
                                                    <gml:pos>3.3974358974 43.8473520249 0.0</gml:pos>
                                                    <gml:pos>3.3974358974 43.8473520249 3.0</gml:pos>
                                                    <gml:pos>30.8974358974 43.8473520249 3.0</gml:pos>
                                                    <gml:pos>30.8974358974 43.8473520249 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY5">
                                            <gml:name>POLY5</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>3.3974358974 43.8473520249 0.0</gml:pos>
                                                    <gml:pos>3.3974358974 17.6012461059 0.0</gml:pos>
                                                    <gml:pos>3.3974358974 17.6012461059 3.0</gml:pos>
                                                    <gml:pos>3.3974358974 43.8473520249 3.0</gml:pos>
                                                    <gml:pos>3.3974358974 43.8473520249 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY6">
                                            <gml:name>POLY6</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>3.3974358974 17.6012461059 0.0</gml:pos>
                                                    <gml:pos>30.8974358974 17.6012461059 0.0</gml:pos>
                                                    <gml:pos>30.8974358974 17.6012461059 3.0</gml:pos>
                                                    <gml:pos>3.3974358974 17.6012461059 3.0</gml:pos>
                                                    <gml:pos>3.3974358974 17.6012461059 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY7">
                                            <gml:name>POLY7</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>30.8974358974 17.6012461059 0.0</gml:pos>
                                                    <gml:pos>30.8974358974 43.8473520249 0.0</gml:pos>
                                                    <gml:pos>30.8974358974 43.8473520249 3.0</gml:pos>
                                                    <gml:pos>30.8974358974 17.6012461059 3.0</gml:pos>
                                                    <gml:pos>30.8974358974 17.6012461059 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY8">
                                            <gml:name>POLY8</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>30.8974358974 43.8473520249 0.0</gml:pos>
                                                    <gml:pos>30.8974358974 17.6012461059 0.0</gml:pos>
                                                    <gml:pos>3.3974358974 17.6012461059 0.0</gml:pos>
                                                    <gml:pos>3.3974358974 43.8473520249 0.0</gml:pos>
                                                    <gml:pos>30.8974358974 43.8473520249 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                </gml:Shell>
                            </gml:exterior>
                        </gml:Solid>
                    </Geometry3D>
                    <duality xlink:href="#R1"/>
                    <partialboundedBy xlink:href="#D1"/>
                    <partialboundedBy xlink:href="#CB11"/>
                    <partialboundedBy xlink:href="#CB12"/>
                </CellSpace>
            </cellSpaceMember>
            <cellSpaceMember>
                <CellSpace gml:id="R2">
                    <gml:description>Usage=Room:Description=Cell R2</gml:description>
                    <gml:name>R2</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <Geometry3D>
                        <gml:Solid gml:id="SOLID2">
                            <gml:exterior>
                                <gml:Shell>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY9">
                                            <gml:name>POLY9</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>30.8974358974 41.199376947 3.0</gml:pos>
                                                    <gml:pos>30.8974358974 17.6012461059 3.0</gml:pos>
                                                    <gml:pos>3.3974358974 17.6012461059 3.0</gml:pos>
                                                    <gml:pos>3.3974358974 7.1651090343 3.0</gml:pos>
                                                    <gml:pos>45.1282051282 7.1651090343 3.0</gml:pos>
                                                    <gml:pos>45.1282051282 41.199376947 3.0</gml:pos>
                                                    <gml:pos>30.8974358974 41.199376947 3.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY10">
                                            <gml:name>POLY10</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>30.8974358974 41.199376947 0.0</gml:pos>
                                                    <gml:pos>30.8974358974 17.6012461059 0.0</gml:pos>
                                                    <gml:pos>30.8974358974 17.6012461059 3.0</gml:pos>
                                                    <gml:pos>30.8974358974 41.199376947 3.0</gml:pos>
                                                    <gml:pos>30.8974358974 41.199376947 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY11">
                                            <gml:name>POLY11</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>30.8974358974 17.6012461059 0.0</gml:pos>
                                                    <gml:pos>3.3974358974 17.6012461059 0.0</gml:pos>
                                                    <gml:pos>3.3974358974 17.6012461059 3.0</gml:pos>
                                                    <gml:pos>30.8974358974 17.6012461059 3.0</gml:pos>
                                                    <gml:pos>30.8974358974 17.6012461059 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY12">
                                            <gml:name>POLY12</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>3.3974358974 17.6012461059 0.0</gml:pos>
                                                    <gml:pos>3.3974358974 7.1651090343 0.0</gml:pos>
                                                    <gml:pos>3.3974358974 7.1651090343 3.0</gml:pos>
                                                    <gml:pos>3.3974358974 17.6012461059 3.0</gml:pos>
                                                    <gml:pos>3.3974358974 17.6012461059 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY13">
                                            <gml:name>POLY13</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>3.3974358974 7.1651090343 0.0</gml:pos>
                                                    <gml:pos>45.1282051282 7.1651090343 0.0</gml:pos>
                                                    <gml:pos>45.1282051282 7.1651090343 3.0</gml:pos>
                                                    <gml:pos>3.3974358974 7.1651090343 3.0</gml:pos>
                                                    <gml:pos>3.3974358974 7.1651090343 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY14">
                                            <gml:name>POLY14</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>45.1282051282 7.1651090343 0.0</gml:pos>
                                                    <gml:pos>45.1282051282 41.199376947 0.0</gml:pos>
                                                    <gml:pos>45.1282051282 41.199376947 3.0</gml:pos>
                                                    <gml:pos>45.1282051282 7.1651090343 3.0</gml:pos>
                                                    <gml:pos>45.1282051282 7.1651090343 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY15">
                                            <gml:name>POLY15</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>45.1282051282 41.199376947 0.0</gml:pos>
                                                    <gml:pos>30.8974358974 41.199376947 0.0</gml:pos>
                                                    <gml:pos>30.8974358974 41.199376947 3.0</gml:pos>
                                                    <gml:pos>45.1282051282 41.199376947 3.0</gml:pos>
                                                    <gml:pos>45.1282051282 41.199376947 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY16">
                                            <gml:name>POLY16</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>30.8974358974 41.199376947 0.0</gml:pos>
                                                    <gml:pos>45.1282051282 41.199376947 0.0</gml:pos>
                                                    <gml:pos>45.1282051282 7.1651090343 0.0</gml:pos>
                                                    <gml:pos>3.3974358974 7.1651090343 0.0</gml:pos>
                                                    <gml:pos>3.3974358974 17.6012461059 0.0</gml:pos>
                                                    <gml:pos>30.8974358974 17.6012461059 0.0</gml:pos>
                                                    <gml:pos>30.8974358974 41.199376947 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                </gml:Shell>
                            </gml:exterior>
                        </gml:Solid>
                    </Geometry3D>
                    <duality xlink:href="#R2"/>
                    <partialboundedBy xlink:href="#D3"/>
                    <partialboundedBy xlink:href="#CB13"/>
                    <partialboundedBy xlink:href="#CB14"/>
                    <partialboundedBy xlink:href="#CB15"/>
                </CellSpace>
            </cellSpaceMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="D3">
                    <gml:description>Usage=Door</gml:description>
                    <gml:name>D3</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:Polygon gml:id="POLY22">
                            <gml:name>POLY22</gml:name>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:pos>25.5128205128 7.1651090343 0.0</gml:pos>
                                    <gml:pos>30.3205128205 7.1651090343 0.0</gml:pos>
                                    <gml:pos>30.3205128205 7.1651090343 2.0</gml:pos>
                                    <gml:pos>25.5128205128 7.1651090343 2.0</gml:pos>
                                    <gml:pos>25.5128205128 7.1651090343 0.0</gml:pos>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="D1">
                    <gml:description>Usage=Door</gml:description>
                    <gml:name>D1</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:Polygon gml:id="POLY18">
                            <gml:name>POLY18</gml:name>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:pos>8.9102564103 17.6012461059 0.0</gml:pos>
                                    <gml:pos>4.4871794872 17.6012461059 0.0</gml:pos>
                                    <gml:pos>4.4871794872 17.6012461059 2.0</gml:pos>
                                    <gml:pos>8.9102564103 17.6012461059 2.0</gml:pos>
                                    <gml:pos>8.9102564103 17.6012461059 0.0</gml:pos>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB11">
                    <gml:name>CB11</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:Polygon gml:id="POLY19">
                            <gml:name>POLY19</gml:name>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:pos>30.8974358974 17.6012461059 0.0</gml:pos>
                                    <gml:pos>8.9102564103 17.6012461059 0.0</gml:pos>
                                    <gml:pos>8.9102564103 17.6012461059 2.0</gml:pos>
                                    <gml:pos>4.4871794872 17.6012461059 2.0</gml:pos>
                                    <gml:pos>4.4871794872 17.6012461059 0.0</gml:pos>
                                    <gml:pos>3.3974358974 17.6012461059 0.0</gml:pos>
                                    <gml:pos>3.3974358974 17.6012461059 3.0</gml:pos>
                                    <gml:pos>30.8974358974 17.6012461059 3.0</gml:pos>
                                    <gml:pos>30.8974358974 17.6012461059 0.0</gml:pos>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB12">
                    <gml:name>CB12</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:Polygon gml:id="POLY20">
                            <gml:name>POLY20</gml:name>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:pos>30.8974358974 41.199376947 0.0</gml:pos>
                                    <gml:pos>30.8974358974 17.6012461059 0.0</gml:pos>
                                    <gml:pos>30.8974358974 17.6012461059 3.0</gml:pos>
                                    <gml:pos>30.8974358974 41.199376947 3.0</gml:pos>
                                    <gml:pos>30.8974358974 41.199376947 0.0</gml:pos>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB13">
                    <gml:name>CB13</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:OrientableSurface gml:id="OrientableSurface1" orientation="-">
                            <gml:baseSurface xlink:href="#POLY20"/>
                        </gml:OrientableSurface>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB14">
                    <gml:name>CB14</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:OrientableSurface gml:id="OrientableSurface2" orientation="-">
                            <gml:baseSurface xlink:href="#POLY19"/>
                        </gml:OrientableSurface>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB15">
                    <gml:name>CB15</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:OrientableSurface gml:id="OrientableSurface3" orientation="-">
                            <gml:baseSurface xlink:href="#POLY18"/>
                        </gml:OrientableSurface>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
        </PrimalSpaceFeatures>
    </primalSpaceFeatures>
    <MultiLayeredGraph gml:id="MLG1">
        <gml:name xsi:nil="true"/>
        <gml:boundedBy xsi:nil="true"/>
        <spaceLayers gml:id="SL1">
            <gml:name>SL1</gml:name>
            <gml:boundedBy xsi:nil="true"/>
            <spaceLayerMember>
                <SpaceLayer gml:id="IS1">
                    <gml:name>IS1</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <nodes gml:id="N1">
                        <gml:name>N1</gml:name>
                        <gml:boundedBy xsi:nil="true"/>
                        <stateMember>
                            <State gml:id="R1">
                                <gml:name>R1</gml:name>
                                <gml:boundedBy xsi:nil="true"/>
                                <duality xlink:href="#R1"/>
                                <geometry>
                                    <gml:Point gml:id="P1">
                                        <gml:name>P1</gml:name>
                                        <gml:pos>17.1474358974 30.7242990654 0.0</gml:pos>
                                    </gml:Point>
                                </geometry>
                            </State>
                        </stateMember>
                        <stateMember>
                            <State gml:id="R2">
                                <gml:name>R2</gml:name>
                                <gml:boundedBy xsi:nil="true"/>
                                <duality xlink:href="#R2"/>
                                <geometry>
                                    <gml:Point gml:id="P2">
                                        <gml:name>P2</gml:name>
                                        <gml:pos>35.0569691312 18.6238499229 0.0</gml:pos>
                                    </gml:Point>
                                </geometry>
                            </State>
                        </stateMember>
                    </nodes>
                    <edges gml:id="E1">
                        <gml:name>E1</gml:name>
                        <gml:boundedBy xsi:nil="true"/>
                    </edges>
                </SpaceLayer>
            </spaceLayerMember>
        </spaceLayers>
    </MultiLayeredGraph>
</IndoorFeatures>
