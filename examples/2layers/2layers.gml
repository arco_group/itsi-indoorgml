<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<IndoorFeatures xmlns="http://www.opengis.net/indoorgml/1.0/core" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:ns4="http://www.opengis.net/indoorgml/1.0/navigation" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" gml:id="IFs" xsi:schemaLocation="http://www.opengis.net/indoorgml/1.0/core http://schemas.opengis.net/indoorgml/1.0/indoorgmlcore.xsd">
    <gml:name>IFs</gml:name>
    <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="EPSG::4326">
            <gml:lowerCorner>2.0612485277 30.0714285714 0.0</gml:lowerCorner>
            <gml:upperCorner>7.5971731449 48.2142857143 3.0</gml:upperCorner>
        </gml:Envelope>
    </gml:boundedBy>
    <primalSpaceFeatures>
        <PrimalSpaceFeatures gml:id="PS1">
            <gml:name>PS1</gml:name>
            <gml:boundedBy xsi:nil="true"/>
            <cellSpaceMember>
                <CellSpace gml:id="C1">
                    <gml:description>Usage=Room</gml:description>
                    <gml:name>C1</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <Geometry3D>
                        <gml:Solid gml:id="SOLID1">
                            <gml:exterior>
                                <gml:Shell>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY6">
                                            <gml:name>POLY6</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>2.0612485277 45.4285714286 3.0</gml:pos>
                                                    <gml:pos>2.0612485277 37.0 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 37.0 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 45.4285714286 3.0</gml:pos>
                                                    <gml:pos>2.0612485277 45.4285714286 3.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY7">
                                            <gml:name>POLY7</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>2.0612485277 45.4285714286 0.0</gml:pos>
                                                    <gml:pos>2.0612485277 37.0 0.0</gml:pos>
                                                    <gml:pos>2.0612485277 37.0 3.0</gml:pos>
                                                    <gml:pos>2.0612485277 45.4285714286 3.0</gml:pos>
                                                    <gml:pos>2.0612485277 45.4285714286 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY8">
                                            <gml:name>POLY8</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>2.0612485277 37.0 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 37.0 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 37.0 3.0</gml:pos>
                                                    <gml:pos>2.0612485277 37.0 3.0</gml:pos>
                                                    <gml:pos>2.0612485277 37.0 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY9">
                                            <gml:name>POLY9</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>5.5653710247 37.0 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 45.4285714286 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 45.4285714286 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 37.0 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 37.0 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY10">
                                            <gml:name>POLY10</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>5.5653710247 45.4285714286 0.0</gml:pos>
                                                    <gml:pos>2.0612485277 45.4285714286 0.0</gml:pos>
                                                    <gml:pos>2.0612485277 45.4285714286 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 45.4285714286 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 45.4285714286 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY11">
                                            <gml:name>POLY11</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>2.0612485277 45.4285714286 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 45.4285714286 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 37.0 0.0</gml:pos>
                                                    <gml:pos>2.0612485277 37.0 0.0</gml:pos>
                                                    <gml:pos>2.0612485277 45.4285714286 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                </gml:Shell>
                            </gml:exterior>
                        </gml:Solid>
                    </Geometry3D>
                    <duality xlink:href="#R1"/>
                    <partialboundedBy xlink:href="#CB4"/>
                    <partialboundedBy xlink:href="#CB39"/>
                    <partialboundedBy xlink:href="#CB40"/>
                    <partialboundedBy xlink:href="#CB41"/>
                </CellSpace>
            </cellSpaceMember>
            <cellSpaceMember>
                <CellSpace gml:id="C2">
                    <gml:description>Usage=Room</gml:description>
                    <gml:name>C2</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <Geometry3D>
                        <gml:Solid gml:id="SOLID2">
                            <gml:exterior>
                                <gml:Shell>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY12">
                                            <gml:name>POLY12</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>5.5653710247 48.2142857143 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 45.4285714286 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 37.0 3.0</gml:pos>
                                                    <gml:pos>2.0612485277 37.0 3.0</gml:pos>
                                                    <gml:pos>2.0612485277 31.9285714286 3.0</gml:pos>
                                                    <gml:pos>7.5971731449 31.9285714286 3.0</gml:pos>
                                                    <gml:pos>7.5971731449 48.2142857143 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 48.2142857143 3.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY13">
                                            <gml:name>POLY13</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>5.5653710247 48.2142857143 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 45.4285714286 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 45.4285714286 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 48.2142857143 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 48.2142857143 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY14">
                                            <gml:name>POLY14</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>5.5653710247 45.4285714286 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 37.0 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 37.0 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 45.4285714286 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 45.4285714286 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY15">
                                            <gml:name>POLY15</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>5.5653710247 37.0 0.0</gml:pos>
                                                    <gml:pos>2.0612485277 37.0 0.0</gml:pos>
                                                    <gml:pos>2.0612485277 37.0 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 37.0 3.0</gml:pos>
                                                    <gml:pos>5.5653710247 37.0 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY16">
                                            <gml:name>POLY16</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>2.0612485277 37.0 0.0</gml:pos>
                                                    <gml:pos>2.0612485277 31.9285714286 0.0</gml:pos>
                                                    <gml:pos>2.0612485277 31.9285714286 3.0</gml:pos>
                                                    <gml:pos>2.0612485277 37.0 3.0</gml:pos>
                                                    <gml:pos>2.0612485277 37.0 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY17">
                                            <gml:name>POLY17</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>2.0612485277 31.9285714286 0.0</gml:pos>
                                                    <gml:pos>7.5971731449 31.9285714286 0.0</gml:pos>
                                                    <gml:pos>7.5971731449 31.9285714286 3.0</gml:pos>
                                                    <gml:pos>2.0612485277 31.9285714286 3.0</gml:pos>
                                                    <gml:pos>2.0612485277 31.9285714286 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY18">
                                            <gml:name>POLY18</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>7.5971731449 31.9285714286 0.0</gml:pos>
                                                    <gml:pos>7.5971731449 48.2142857143 0.0</gml:pos>
                                                    <gml:pos>7.5971731449 48.2142857143 3.0</gml:pos>
                                                    <gml:pos>7.5971731449 31.9285714286 3.0</gml:pos>
                                                    <gml:pos>7.5971731449 31.9285714286 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY19">
                                            <gml:name>POLY19</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>7.5971731449 48.2142857143 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 48.2142857143 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 48.2142857143 3.0</gml:pos>
                                                    <gml:pos>7.5971731449 48.2142857143 3.0</gml:pos>
                                                    <gml:pos>7.5971731449 48.2142857143 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY20">
                                            <gml:name>POLY20</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>5.5653710247 48.2142857143 0.0</gml:pos>
                                                    <gml:pos>7.5971731449 48.2142857143 0.0</gml:pos>
                                                    <gml:pos>7.5971731449 31.9285714286 0.0</gml:pos>
                                                    <gml:pos>2.0612485277 31.9285714286 0.0</gml:pos>
                                                    <gml:pos>2.0612485277 37.0 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 37.0 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 45.4285714286 0.0</gml:pos>
                                                    <gml:pos>5.5653710247 48.2142857143 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                </gml:Shell>
                            </gml:exterior>
                        </gml:Solid>
                    </Geometry3D>
                    <duality xlink:href="#R2"/>
                    <partialboundedBy xlink:href="#CB42"/>
                    <partialboundedBy xlink:href="#CB43"/>
                    <partialboundedBy xlink:href="#CB44"/>
                    <partialboundedBy xlink:href="#CB45"/>
                </CellSpace>
            </cellSpaceMember>
            <cellSpaceMember>
                <CellSpace gml:id="celdaCobertura">
                    <gml:description>Usage=Room</gml:description>
                    <gml:name>celdaCobertura</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <Geometry3D>
                        <gml:Solid gml:id="SOLID3">
                            <gml:exterior>
                                <gml:Shell>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY30">
                                            <gml:name>POLY30</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>3.9163722026 43.2142857143 3.0</gml:pos>
                                                    <gml:pos>3.0035335689 39.1428571429 3.0</gml:pos>
                                                    <gml:pos>3.9163722026 34.8571428571 3.0</gml:pos>
                                                    <gml:pos>7.0965842167 36.6428571429 3.0</gml:pos>
                                                    <gml:pos>6.124852768 42.5 3.0</gml:pos>
                                                    <gml:pos>3.9163722026 43.2142857143 3.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY31">
                                            <gml:name>POLY31</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>3.9163722026 43.2142857143 0.0</gml:pos>
                                                    <gml:pos>3.0035335689 39.1428571429 0.0</gml:pos>
                                                    <gml:pos>3.0035335689 39.1428571429 3.0</gml:pos>
                                                    <gml:pos>3.9163722026 43.2142857143 3.0</gml:pos>
                                                    <gml:pos>3.9163722026 43.2142857143 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY32">
                                            <gml:name>POLY32</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>3.0035335689 39.1428571429 0.0</gml:pos>
                                                    <gml:pos>3.9163722026 34.8571428571 0.0</gml:pos>
                                                    <gml:pos>3.9163722026 34.8571428571 3.0</gml:pos>
                                                    <gml:pos>3.0035335689 39.1428571429 3.0</gml:pos>
                                                    <gml:pos>3.0035335689 39.1428571429 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY33">
                                            <gml:name>POLY33</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>3.9163722026 34.8571428571 0.0</gml:pos>
                                                    <gml:pos>7.0965842167 36.6428571429 0.0</gml:pos>
                                                    <gml:pos>7.0965842167 36.6428571429 3.0</gml:pos>
                                                    <gml:pos>3.9163722026 34.8571428571 3.0</gml:pos>
                                                    <gml:pos>3.9163722026 34.8571428571 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY34">
                                            <gml:name>POLY34</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>7.0965842167 36.6428571429 0.0</gml:pos>
                                                    <gml:pos>6.124852768 42.5 0.0</gml:pos>
                                                    <gml:pos>6.124852768 42.5 3.0</gml:pos>
                                                    <gml:pos>7.0965842167 36.6428571429 3.0</gml:pos>
                                                    <gml:pos>7.0965842167 36.6428571429 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY35">
                                            <gml:name>POLY35</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>6.124852768 42.5 0.0</gml:pos>
                                                    <gml:pos>3.9163722026 43.2142857143 0.0</gml:pos>
                                                    <gml:pos>3.9163722026 43.2142857143 3.0</gml:pos>
                                                    <gml:pos>6.124852768 42.5 3.0</gml:pos>
                                                    <gml:pos>6.124852768 42.5 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY36">
                                            <gml:name>POLY36</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>3.9163722026 43.2142857143 0.0</gml:pos>
                                                    <gml:pos>6.124852768 42.5 0.0</gml:pos>
                                                    <gml:pos>7.0965842167 36.6428571429 0.0</gml:pos>
                                                    <gml:pos>3.9163722026 34.8571428571 0.0</gml:pos>
                                                    <gml:pos>3.0035335689 39.1428571429 0.0</gml:pos>
                                                    <gml:pos>3.9163722026 43.2142857143 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                </gml:Shell>
                            </gml:exterior>
                        </gml:Solid>
                    </Geometry3D>
                    <duality xlink:href="#R3"/>
                    <partialboundedBy xlink:href="#CB30"/>
                    <partialboundedBy xlink:href="#CB46"/>
                    <partialboundedBy xlink:href="#CB47"/>
                </CellSpace>
            </cellSpaceMember>
            <cellSpaceMember>
                <CellSpace gml:id="C4">
                    <gml:description>Usage=Room</gml:description>
                    <gml:name>C4</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <Geometry3D>
                        <gml:Solid gml:id="SOLID4">
                            <gml:exterior>
                                <gml:Shell>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY37">
                                            <gml:name>POLY37</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>7.0965842167 36.6428571429 3.0</gml:pos>
                                                    <gml:pos>3.9163722026 34.8571428571 3.0</gml:pos>
                                                    <gml:pos>4.8420901951 30.0714285714 3.0</gml:pos>
                                                    <gml:pos>7.0965842167 32.1428571429 3.0</gml:pos>
                                                    <gml:pos>7.0965842167 36.6428571429 3.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY38">
                                            <gml:name>POLY38</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>7.0965842167 36.6428571429 0.0</gml:pos>
                                                    <gml:pos>3.9163722026 34.8571428571 0.0</gml:pos>
                                                    <gml:pos>3.9163722026 34.8571428571 3.0</gml:pos>
                                                    <gml:pos>7.0965842167 36.6428571429 3.0</gml:pos>
                                                    <gml:pos>7.0965842167 36.6428571429 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY39">
                                            <gml:name>POLY39</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>3.9163722026 34.8571428571 0.0</gml:pos>
                                                    <gml:pos>4.8420901951 30.0714285714 0.0</gml:pos>
                                                    <gml:pos>4.8420901951 30.0714285714 3.0</gml:pos>
                                                    <gml:pos>3.9163722026 34.8571428571 3.0</gml:pos>
                                                    <gml:pos>3.9163722026 34.8571428571 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY40">
                                            <gml:name>POLY40</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>4.8420901951 30.0714285714 0.0</gml:pos>
                                                    <gml:pos>7.0965842167 32.1428571429 0.0</gml:pos>
                                                    <gml:pos>7.0965842167 32.1428571429 3.0</gml:pos>
                                                    <gml:pos>4.8420901951 30.0714285714 3.0</gml:pos>
                                                    <gml:pos>4.8420901951 30.0714285714 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY41">
                                            <gml:name>POLY41</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>7.0965842167 32.1428571429 0.0</gml:pos>
                                                    <gml:pos>7.0965842167 36.6428571429 0.0</gml:pos>
                                                    <gml:pos>7.0965842167 36.6428571429 3.0</gml:pos>
                                                    <gml:pos>7.0965842167 32.1428571429 3.0</gml:pos>
                                                    <gml:pos>7.0965842167 32.1428571429 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="POLY42">
                                            <gml:name>POLY42</gml:name>
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:pos>7.0965842167 36.6428571429 0.0</gml:pos>
                                                    <gml:pos>7.0965842167 32.1428571429 0.0</gml:pos>
                                                    <gml:pos>4.8420901951 30.0714285714 0.0</gml:pos>
                                                    <gml:pos>3.9163722026 34.8571428571 0.0</gml:pos>
                                                    <gml:pos>7.0965842167 36.6428571429 0.0</gml:pos>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                </gml:Shell>
                            </gml:exterior>
                        </gml:Solid>
                    </Geometry3D>
                    <duality xlink:href="#R4"/>
                    <partialboundedBy xlink:href="#CB48"/>
                    <partialboundedBy xlink:href="#CB49"/>
                    <partialboundedBy xlink:href="#CB50"/>
                </CellSpace>
            </cellSpaceMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB4">
                    <gml:description>Usage=Door</gml:description>
                    <gml:name>CB4</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <duality xlink:href="#T1"/>
                    <geometry3D>
                        <gml:Polygon gml:id="POLY22">
                            <gml:name>POLY22</gml:name>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:pos>2.7090694935 37.0 0.0</gml:pos>
                                    <gml:pos>3.5335689046 37.0 0.0</gml:pos>
                                    <gml:pos>3.5335689046 37.0 2.0</gml:pos>
                                    <gml:pos>2.7090694935 37.0 2.0</gml:pos>
                                    <gml:pos>2.7090694935 37.0 0.0</gml:pos>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB39">
                    <gml:name>CB39</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:Polygon gml:id="POLY23">
                            <gml:name>POLY23</gml:name>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:pos>3.5335689046 37.0 0.0</gml:pos>
                                    <gml:pos>5.5653710247 37.0 0.0</gml:pos>
                                    <gml:pos>5.5653710247 37.0 3.0</gml:pos>
                                    <gml:pos>3.5335689046 37.0 3.0</gml:pos>
                                    <gml:pos>3.5335689046 37.0 0.0</gml:pos>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB40">
                    <gml:name>CB40</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:Polygon gml:id="POLY24">
                            <gml:name>POLY24</gml:name>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:pos>2.0612485277 37.0 0.0</gml:pos>
                                    <gml:pos>2.7090694935 37.0 0.0</gml:pos>
                                    <gml:pos>2.7090694935 37.0 3.0</gml:pos>
                                    <gml:pos>2.0612485277 37.0 3.0</gml:pos>
                                    <gml:pos>2.0612485277 37.0 0.0</gml:pos>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB41">
                    <gml:name>CB41</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:Polygon gml:id="POLY25">
                            <gml:name>POLY25</gml:name>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:pos>5.5653710247 37.0 0.0</gml:pos>
                                    <gml:pos>5.5653710247 45.4285714286 0.0</gml:pos>
                                    <gml:pos>5.5653710247 45.4285714286 3.0</gml:pos>
                                    <gml:pos>5.5653710247 37.0 3.0</gml:pos>
                                    <gml:pos>5.5653710247 37.0 0.0</gml:pos>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB42">
                    <gml:name>CB42</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:OrientableSurface gml:id="OrientableSurface1" orientation="-">
                            <gml:baseSurface xlink:href="#POLY25"/>
                        </gml:OrientableSurface>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB43">
                    <gml:name>CB43</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:OrientableSurface gml:id="OrientableSurface2" orientation="-">
                            <gml:baseSurface xlink:href="#POLY23"/>
                        </gml:OrientableSurface>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB44">
                    <gml:name>CB44</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <duality xlink:href="#T5"/>
                    <geometry3D>
                        <gml:OrientableSurface gml:id="OrientableSurface3" orientation="-">
                            <gml:baseSurface xlink:href="#POLY22"/>
                        </gml:OrientableSurface>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB45">
                    <gml:name>CB45</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:OrientableSurface gml:id="OrientableSurface4" orientation="-">
                            <gml:baseSurface xlink:href="#POLY24"/>
                        </gml:OrientableSurface>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB30">
                    <gml:description>Usage=Door</gml:description>
                    <gml:name>CB30</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <duality xlink:href="#T4"/>
                    <geometry3D>
                        <gml:Polygon gml:id="POLY44">
                            <gml:name>POLY44</gml:name>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:pos>4.9957655064 35.4632307639 0.0</gml:pos>
                                    <gml:pos>5.8021403649 35.9160166467 0.0</gml:pos>
                                    <gml:pos>5.8021403649 35.9160166467 2.0</gml:pos>
                                    <gml:pos>4.9957655064 35.4632307639 2.0</gml:pos>
                                    <gml:pos>4.9957655064 35.4632307639 0.0</gml:pos>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB46">
                    <gml:name>CB46</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:Polygon gml:id="POLY45">
                            <gml:name>POLY45</gml:name>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:pos>5.8021403649 35.9160166467 0.0</gml:pos>
                                    <gml:pos>7.0965842167 36.6428571429 0.0</gml:pos>
                                    <gml:pos>7.0965842167 36.6428571429 3.0</gml:pos>
                                    <gml:pos>5.8021403649 35.9160166467 3.0</gml:pos>
                                    <gml:pos>5.8021403649 35.9160166467 0.0</gml:pos>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB47">
                    <gml:name>CB47</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:Polygon gml:id="POLY46">
                            <gml:name>POLY46</gml:name>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:pos>3.9163722026 34.8571428571 0.0</gml:pos>
                                    <gml:pos>4.9957655064 35.4632307639 0.0</gml:pos>
                                    <gml:pos>4.9957655064 35.4632307639 3.0</gml:pos>
                                    <gml:pos>3.9163722026 34.8571428571 3.0</gml:pos>
                                    <gml:pos>3.9163722026 34.8571428571 0.0</gml:pos>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB48">
                    <gml:name>CB48</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:OrientableSurface gml:id="OrientableSurface5" orientation="-">
                            <gml:baseSurface xlink:href="#POLY45"/>
                        </gml:OrientableSurface>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB49">
                    <gml:name>CB49</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <duality xlink:href="#T6"/>
                    <geometry3D>
                        <gml:OrientableSurface gml:id="OrientableSurface6" orientation="-">
                            <gml:baseSurface xlink:href="#POLY44"/>
                        </gml:OrientableSurface>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
            <cellSpaceBoundaryMember>
                <CellSpaceBoundary gml:id="CB50">
                    <gml:name>CB50</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <geometry3D>
                        <gml:OrientableSurface gml:id="OrientableSurface7" orientation="-">
                            <gml:baseSurface xlink:href="#POLY46"/>
                        </gml:OrientableSurface>
                    </geometry3D>
                </CellSpaceBoundary>
            </cellSpaceBoundaryMember>
        </PrimalSpaceFeatures>
    </primalSpaceFeatures>
    <MultiLayeredGraph gml:id="MLG1">
        <gml:name xsi:nil="true"/>
        <gml:boundedBy xsi:nil="true"/>
        <spaceLayers gml:id="SL1">
            <gml:name>SL1</gml:name>
            <gml:boundedBy xsi:nil="true"/>
            <spaceLayerMember>
                <SpaceLayer gml:id="topo">
                    <gml:name>topo</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <nodes gml:id="N1">
                        <gml:name>N1</gml:name>
                        <gml:boundedBy xsi:nil="true"/>
                        <stateMember>
                            <State gml:id="R1">
                                <gml:name>R1</gml:name>
                                <gml:boundedBy xsi:nil="true"/>
                                <duality xlink:href="#C1"/>
                                <connects xlink:href="#T1"/>
                                <connects xlink:href="#T5"/>
                                <geometry>
                                    <gml:Point gml:id="P1">
                                        <gml:name>P1</gml:name>
                                        <gml:pos>3.8133097762 41.2142857143 0.0</gml:pos>
                                    </gml:Point>
                                </geometry>
                            </State>
                        </stateMember>
                        <stateMember>
                            <State gml:id="R2">
                                <gml:name>R2</gml:name>
                                <gml:boundedBy xsi:nil="true"/>
                                <duality xlink:href="#C2"/>
                                <connects xlink:href="#T1"/>
                                <connects xlink:href="#T5"/>
                                <geometry>
                                    <gml:Point gml:id="P2">
                                        <gml:name>P2</gml:name>
                                        <gml:pos>6.3208416804 34.1836846521 0.0</gml:pos>
                                    </gml:Point>
                                </geometry>
                            </State>
                        </stateMember>
                    </nodes>
                    <edges gml:id="E1">
                        <gml:name>E1</gml:name>
                        <gml:boundedBy xsi:nil="true"/>
                        <transitionMember>
                            <Transition gml:id="T1">
                                <gml:name>T1</gml:name>
                                <gml:boundedBy xsi:nil="true"/>
                                <weight>1.0</weight>
                                <connects xlink:href="#R1"/>
                                <connects xlink:href="#R2"/>
                                <duality xlink:href="#CB4"/>
                                <geometry>
                                    <gml:LineString gml:id="LS1">
                                        <gml:pos>3.8133097762 41.2142857143 0.0</gml:pos>
                                        <gml:pos>3.1507656066 38.4285714286 0.0</gml:pos>
                                        <gml:pos>3.0918727915 34.4285714286 0.0</gml:pos>
                                        <gml:pos>6.3208416804 34.1836846521 0.0</gml:pos>
                                    </gml:LineString>
                                </geometry>
                            </Transition>
                        </transitionMember>
                        <transitionMember>
                            <Transition gml:id="T5">
                                <gml:name>T5</gml:name>
                                <gml:boundedBy xsi:nil="true"/>
                                <weight>1.0</weight>
                                <connects xlink:href="#R1"/>
                                <connects xlink:href="#R2"/>
                                <duality xlink:href="#CB44"/>
                                <geometry>
                                    <gml:LineString gml:id="LS2">
                                        <gml:pos>3.8133097762 41.2142857143 0.0</gml:pos>
                                        <gml:pos>3.1507656066 38.4285714286 0.0</gml:pos>
                                        <gml:pos>3.0918727915 34.4285714286 0.0</gml:pos>
                                        <gml:pos>6.3208416804 34.1836846521 0.0</gml:pos>
                                    </gml:LineString>
                                </geometry>
                            </Transition>
                        </transitionMember>
                    </edges>
                </SpaceLayer>
            </spaceLayerMember>
            <spaceLayerMember>
                <SpaceLayer gml:id="cober">
                    <gml:name>cober</gml:name>
                    <gml:boundedBy xsi:nil="true"/>
                    <nodes gml:id="N2">
                        <gml:name>N2</gml:name>
                        <gml:boundedBy xsi:nil="true"/>
                        <stateMember>
                            <State gml:id="R3">
                                <gml:name>R3</gml:name>
                                <gml:boundedBy xsi:nil="true"/>
                                <duality xlink:href="#celdaCobertura"/>
                                <connects xlink:href="#T4"/>
                                <connects xlink:href="#T6"/>
                                <geometry>
                                    <gml:Point gml:id="P3">
                                        <gml:name>P3</gml:name>
                                        <gml:pos>4.9463724154 39.0463931531 0.0</gml:pos>
                                    </gml:Point>
                                </geometry>
                            </State>
                        </stateMember>
                        <stateMember>
                            <State gml:id="R4">
                                <gml:name>R4</gml:name>
                                <gml:boundedBy xsi:nil="true"/>
                                <duality xlink:href="#C4"/>
                                <connects xlink:href="#T4"/>
                                <connects xlink:href="#T6"/>
                                <geometry>
                                    <gml:Point gml:id="P4">
                                        <gml:name>P4</gml:name>
                                        <gml:pos>5.6830724786 33.5174044176 0.0</gml:pos>
                                    </gml:Point>
                                </geometry>
                            </State>
                        </stateMember>
                    </nodes>
                    <edges gml:id="E2">
                        <gml:name>E2</gml:name>
                        <gml:boundedBy xsi:nil="true"/>
                        <transitionMember>
                            <Transition gml:id="T4">
                                <gml:name>T4</gml:name>
                                <gml:boundedBy xsi:nil="true"/>
                                <weight>1.0</weight>
                                <connects xlink:href="#R4"/>
                                <connects xlink:href="#R3"/>
                                <duality xlink:href="#CB30"/>
                                <geometry>
                                    <gml:LineString gml:id="LS3">
                                        <gml:pos>5.6830724786 33.5174044176 0.0</gml:pos>
                                        <gml:pos>4.9463724154 39.0463931531 0.0</gml:pos>
                                    </gml:LineString>
                                </geometry>
                            </Transition>
                        </transitionMember>
                        <transitionMember>
                            <Transition gml:id="T6">
                                <gml:name>T6</gml:name>
                                <gml:boundedBy xsi:nil="true"/>
                                <weight>1.0</weight>
                                <connects xlink:href="#R4"/>
                                <connects xlink:href="#R3"/>
                                <duality xlink:href="#CB49"/>
                                <geometry>
                                    <gml:LineString gml:id="LS4">
                                        <gml:pos>5.6830724786 33.5174044176 0.0</gml:pos>
                                        <gml:pos>4.9463724154 39.0463931531 0.0</gml:pos>
                                    </gml:LineString>
                                </geometry>
                            </Transition>
                        </transitionMember>
                    </edges>
                </SpaceLayer>
            </spaceLayerMember>
        </spaceLayers>
    </MultiLayeredGraph>
</IndoorFeatures>
